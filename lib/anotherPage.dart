import 'package:flutter/material.dart';
import 'package:unddemy_task_8/main.dart';

class AnotherPage extends StatelessWidget {
  static final String route = '/another-page';
  final String text;
  final String subtext;
  AnotherPage({
    this.text,
    this.subtext,
  });
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(child: Text(text)),
          SizedBox(child: Text(subtext)),
          Container(
            width: double.infinity,
            height: 100,
            child: RaisedButton(
              color: Colors.red,
              onPressed: () => Navigator.pushNamed(context, MyHomePage.route,
                  arguments: [
                    'From AnotherPage to HomePage page',
                    'No, u do not'
                  ]),
            ),
          ),
        ],
      ),
    );
  }
}
