import 'package:flutter/material.dart';
import 'package:unddemy_task_8/anotherPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      onGenerateRoute: (setting) {
        List<String> list = setting.arguments as List;
        if (setting.name == AnotherPage.route) {
          print('=> AnotherPage ${setting.arguments}');
          return MaterialPageRoute(
              builder: (ctx) => AnotherPage(
                    text: list[0] ?? '',
                    subtext: list[1] ?? '',
                  ));
        }
        print('=> MyHomePage ${setting.arguments}');
        return MaterialPageRoute(
            builder: (ctx) => MyHomePage(
                  text: list[0] ?? '',
                  subtext: list[1] ?? '',
                ));
      },
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String text;
  final String subtext;

  static final String route = '/home-page';
  MyHomePage({
    this.text,
    this.subtext,
  });
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(text ?? ''),
        Text(subtext ?? ''),
        Container(
          width: double.infinity,
          height: 100,
          child: RaisedButton(
              color: Colors.green,
              onPressed: () => Navigator.pushNamed(context, AnotherPage.route,
                      arguments: [
                        'From HomePage to AnotherPage',
                        'Yep, i am unique'
                      ])),
        ),
      ]),
    );
  }
}
